const fs = require('fs-extra');
const moment = require('moment');
const YAML = require('yamljs');

let logPath = '';
let logItems = [];
let logItem = null;

async function startReport() {
	const dirExists = await fs.exists('./reports');

	if (!dirExists) {
		await fs.mkdir('./reports');
	}

	const printsDirExists = await fs.exists('./prints');

	if (!printsDirExists) {
		await fs.mkdir('./prints');
	}
	logPath = `./reports/report-${moment().format('DD-MM-YYYY_HH-mm-ss')}.html`;
	await fs.createFile(logPath);

	fs.appendFile(
		logPath,
		`
        <html>
        <head>
            <title> Log :D </title>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    
        </head>
        <body>

        <h1> Log de permissao </h1>

        <table class="table table-striped  table-bordered">
            <thead  class="thead-dark">
                <tr>
                    <td> Action </td>
                    <td> Step </td>
                    <td> Screenshot </td>
                    <td> tempo </td>
                    <td> extra </td>
                </tr>
            </thead>
            <tbody>
    `
	);
}

async function logStep(action, step, screenshot, tempo, extra) {
	logItem = { action, step, screenshot, dados: [] };
	logItems.push(logItem);

	await fs.appendFile(
		logPath,
		`

         
        <tr>
            <td> ${action} </td>
            <td> ${step} </td>
            <td> <a href=".${screenshot}" target="_BLANK" > <img style="max-width: 200px; max-height: 200px" class="img-thumbnail rounded" src=".${screenshot}" /> </a> </td>
            <td> ${tempo} </td>
            <td> <pre>${YAML.stringify(extra || {}, 4)}</pre> </td>
        </tr>
            
    `
	);
}

async function closeReport() {
	await fs.appendFile(
		logPath,
		`

         
             
            </tbody>
        </table>
        </body> 
            
    `
	);
}

module.exports = {
	startReport,
	logStep,
	closeReport
};
