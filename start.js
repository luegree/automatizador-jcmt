const puppeteer = require('puppeteer');
const luxon = require('luxon');
const { startReport, logStep, closeReport } = require('./report');

require('./stuff');

const actions = require('./actions');
let printId = 1;
console.log(actions);

async function printDaMaldade(page, stepName = 'semnome') {
	const caminho = './prints/' + printId++ + '.' + stepName + '.' + new Date().valueOf() + '.png';
	await page.screenshot({
		path: caminho,
		fullPage: true
	});
	return caminho;
}

async function start() {
	await startReport();

	const browser = await puppeteer.launch();

	const page = await browser.newPage();

	await page.goto('http://portalservicos.jucemat.mt.gov.br/PortalInterno');

	const state = {};

	for (let action of actions) {
		do {
			state.extra = {};
			await esperar(300);

			const steps = await action(page);
			state.repetir = false;
			state.stop = false;

			await (async function() {
				for (let step of steps) {
					if (state.stop) break;
					await esperar(300);
					let inicio = luxon.DateTime.fromJSDate(new Date());
					console.log(`[${action.name} -> ${step.name}] Iniciando...`);

					try {
						await step(state);
					} catch (e) {
						state.extra['Erro'] = e + '';
					}
					let diferenca = luxon.DateTime.fromJSDate(new Date()).diff(inicio);
					let tempo = diferenca.toFormat('mm:ss.SSS').toString();
					console.log(`[${action.name} -> ${step.name}] Finalizada... (duracao ${tempo})`);
					const printFile = await printDaMaldade(page, action.name + '.' + step.name + (state.name || ''));
					console.log(`[${action.name} -> ${step.name}] Print feito!`);

					await logStep(action.name, step.name, printFile, tempo, state.extra || {});
				}
			})();
		} while (state.repetir && !state.abort);
	}

    closeReport();
    
    process.exit();
}

start();
