const fs = require('fs-extra');

module.exports = async function trocarPermissao(page) {
	async function recuperarLogins(state) {
		state.repetir = true;

		if (state.logins) {
			return;
		}

		const file = await fs.readFile('./logins.txt', 'utf8');

		const logins = file.split('\n').map((x) => x.trim());

		state.logins = logins;
		state.loginIndex = 0;
		state.extra['Logins recuperados'] = state.logins;
		console.log(state);
	}

	async function pesquisarLogin(state) {
		if (state.loginIndex >= state.logins.length) {
			state.stop = true;
			state.abort = true;
			return;
		}

		state.extra['Login Selecionado'] = state.logins[state.loginIndex];
		const login = state.logins[state.loginIndex];

		const input = await page.$('#inputLogin');
		await input.focus();

		await page.evaluate((i) => (i.value = ''), input);

		await page.keyboard.type(login, { delay: 100 }); // Types instantly
		await page.keyboard.press('Enter');
	}

	async function clicarNoLogin(state) {
		const link = await page.$('#panelListaUsuarios a');

		await link.click();
	}

	async function verificarPermissao(state) {
		const bodyHandle = await page.$('body');
		const [ temInput, nome ] = await page.evaluate((body) => {
			const inputNome = body.querySelector('#nomeUser');
			const inputs = [].slice.call(body.querySelectorAll('#gruposPanel .grupo_user'));
			const temInput = inputs.filter((x) => x.value.startsWith('DOCUMENTODIGITALIZADO-IMPRIME - IJRM'))[0];
			return [ !!temInput, inputNome.value ];
		}, bodyHandle);

		state.extra['Nome'] = nome;
		state.extra['Usuário já tinha permissão'] = temInput ? 'Sim' : 'Não';
		state.preffix = nome;

		if (temInput) {
			console.log('o usuario ' + nome + ' já tem a permissão');
			state.stop = true;
			state.loginIndex++;
			await voltar();
		}
	}

	async function voltar() {
		const botaoVoltar = await page.$('#cancelaUsuario');
		await botaoVoltar.click();
	}

	async function pressionarTrocarPermissao() {
		const bodyHandle = await page.$('body');
		await page.evaluate((body) => {
			const inputs = [].slice.call(body.querySelectorAll('#gruposPanel .grupo_user'));

			const inputCerto = inputs.filter((x) => x.value.startsWith('DOCUMENTODIGITALIZADO - IJRM'))[0];
			const posicaoInput = inputs.indexOf(inputCerto);

			const botoes = document.querySelectorAll('#gruposPanel .acao_pesquisar');

			const botaoCerto = botoes[posicaoInput];

			botaoCerto.click();
		}, bodyHandle);
	}

	async function pesquisarPermissao() {
		const dropdown = await page.$('#selectCodigoAplicacao');
		const campo = await page.$('#descricaoGrupo');
		const botao = await page.$('#pesquisaGrupo');

		await page.evaluate((dropdown) => (dropdown.value = 'IJRM'), dropdown);
		await page.evaluate((campo) => (campo.value = 'DOCUMENTO'), campo);
		await botao.click();
	}

	async function selecionarPermissao() {
		var links = await page.$$('#panelListaGrupos a');

		var linkCerto = links[1];

		await linkCerto.click();
	}

	async function salvar(state) {
		const botao = await page.$('#gravaUsuario');
		await botao.click();

		state.loginIndex++;
	}

	async function voltarParaListagem() {
		state.loginIndex++;

		await voltar();
	}

	return [
		recuperarLogins,
		pesquisarLogin,
		clicarNoLogin,
		verificarPermissao,
		pressionarTrocarPermissao,
		pesquisarPermissao,
		selecionarPermissao,
		salvar
	];
};
