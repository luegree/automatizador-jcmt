module.exports = async function entrar(page) {
	async function digitarLogin(state) {
		const input = await page.$('#username');
		input.focus();
		await page.keyboard.type('04030073131', { delay: 100 }); // Types instantly
		state.extra['Login com Usuario'] = '04030073131';
	}

	async function digitarSenha() {
		const input = await page.$('#password');
		input.focus();
		await page.keyboard.type('3b3mrp52w4', { delay: 100 }); // Types instantly
	}

	async function apertarBotaoEntrar(state) {
		try {
			const botao = await page.$('#kc-login');
			botao.click();

			await esperar(5000);

			state.extra['Login com Sucesso'] = true;
		} catch (e) {
			state.extra['Login com Sucesso'] = false;
			state.stop = true;
		}
	}

	async function selecionarPortalInterno() {
		const botoes = await page.$$('.div_inline');
		const botaoPortalInterno = botoes[1];
		const link = await botaoPortalInterno.$('a');
		await link.click();
	}

	async function selecionarUsuarios() {
		const botoes = await page.$$('.div_inline');
		const botaoPortalInterno = botoes[0];
		const link = await botaoPortalInterno.$('a');
		await link.click();
	}

	return [ digitarLogin, digitarSenha, apertarBotaoEntrar, selecionarPortalInterno, selecionarUsuarios ];
};
