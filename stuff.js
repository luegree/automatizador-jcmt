global.esperar = function esperar(time) {
	return new Promise((resolve, reject) => {
		setTimeout(resolve, time);
	});
};
